window.onload=function(){

  Vue.component('input-date', {  // system for the date input
		template: '\
		    <input\
		      type="date"\
		      ref="input"\
		      v-bind:value="dateToYYYYMMDD(value)"\
		      v-on:input="updateValue($event.target)"\
		      v-on:focus="selectAll"\
		    >\
		',
		props: {
		  value: {
		    type: Date,
		    default: new Date()
		  }
		},
		methods: {
		  dateToYYYYMMDD:function(date) {  // may have timezone caveats https://stackoverflow.com/a/29774197/1850609
		    return date && date.toISOString().split('T')[0];
		  },
		  updateValue: function (target) {
		    this.$emit('input', target.valueAsDate);
		  },
		  selectAll: function (event) {
		    setTimeout(function () {
		    	event.target.select();
		    }, 0);
		  }
		}
	});


  var date = new Vue({  // method around the time input
		el: '#date',
		data: {  // contain the 2 date
		  date_list: [
			{ name: "Start date: ", date: new Date('2010-01-01T00:00:00Z'),
					evolution: -1, button_text: "Remove a day",
					title: "the limit of the time interval" },  // an early date
				{ name: "End date: ", date: new Date(),
					evolution: 1, button_text: "Add a day",
					title: "the most recent date" }  // the current date
		  ]
		},
		methods: {
			set_today: function (element) {
			element.date = new Date();
		  },
		  add_day: function (element) {
			if (element.date) // as myDate can be null
			  // you have to set the this.myDate again, so vue can detect it changed
			  // this is not a caveat of this specific solution, but of any binding of dates
			  element.date = new Date(element.date.setDate(element.date.getDate() + element.evolution));
		  }
		}
	});

  var case_name = new Vue({  // method around the choice of the case name (the type of test)
	el: "#case_name",
	data: {
	  choice: [],  // contain the names choosen
	  list: [
		{ name: "onap-k8s" },
		{ name: "onap-helm" },
		{ name: "core" },
		{ name: "full" },
		{ name: "healthdist" },
		{ name: "postinstall" },
    		{ name: "dcaemod" },
    		{ name: "ves-collector" },
    		{ name: "cmpv2" },
    		{ name: "hv-ves" },
		{ name: "basic_onboard" },
		{ name: "basic_cds" },
		{ name: "basic_clamp" },
		{ name: "basic_vm" },
		{ name: "basic_vm_macro" },
		{ name: "pnf-registrate" },
		{ name: "5gbulkpm" },
    		{ name: "basic_cnf" },
    		{ name: "basic_network" },
    		{ name: "root_pods" },
    		{ name: "jdpw_ports" },
    		{ name: "nonssl_endpoints" },
    		{ name: "unlimitted_pods" },
	  ]  // list of choice
	},
	methods: {
	  change: function(name){  // select cases name
		contain = -1;
		for (i in this.choice) {
			  if (this.choice[i] == name){
				contain = i;
			}
		}

		if (contain != -1){  // remove the name if it is already in choice
		  this.choice.splice(contain, 1);
		}
		else{
		  this.choice.push(name);  // add it to the list of name_case
		}
	  },
	  get_element: function(name) {  // return a true if the name is in choice
		contain = false;
		for (i in this.choice) {
			  if (this.choice[i] == name){
				contain = true;
			}
		}
		return contain;
	  }
	}
  });

  var option = new Vue({  // methods containing both the option and the number of data
	el: "#option",
	data: {
	  selector_pod: { description: "Gating", id: "onap_oom_gating_pod4_1-OPNFV-oom" },  // caution, it is a copy of list_pod[0]
	  list_pod: [
		{ description: "DT Master daily", id: "onap-master-daily-dell-ONAPatDT-oom" },
		{ description: "DT Honolulu daily", id: "onap-honolulu-daily-hp-ONAPatDT-oom" },
		{ description: "DT Guilin daily", id: "onap-guilin-daily-hp-ONAPatDT-oom" },
		{ description: "DT Frankfurt daily", id: "onap-frankfurt-daily-hp-ONAPatDT-oom" },
		{ description: "Orange Master daily", id: "onap_daily_pod4_master-ONAP-oom" },
		{ description: "Orange Master weekly", id: "onap_weekly_pod4_master-ONAP-oom" },
		{ description: "Orange Honolulu daily", id: "onap_daily_pod4_honolulu-ONAP-oom" },	
		{ description: "Orange Honolulu weekly", id: "onap_weekly_pod4_honolulu-ONAP-oom" },	
    		{ description: "Orange Guilin daily", id: "onap_daily_pod4_guilin-ONAP-oom" },
		{ description: "Orange Frankfurt daily", id: "onap_daily_pod4_frankfurt-ONAP-oom" },
		{ description: "Nokia dual stack", id: "ci-dualstack-master-daily" }
	  ],
	  list_test: [
		{ description: "Test results in time series", id: "diagram_pod", title: "all tests" },  // list of option for the time serie
		{ description: "Smoothed test result", id: "average_pod", title: "average of the daily tests" },
		{ description: "Smoothed test criteria", id: "average_success", title: "average of the daily tests" }
	  ],
	  selector_test: { description: "Test results in time series", id: "diagram_pod", title: "all tests" },  // caution, it is a copy of list_test[0]
	  nb_data: 20,
	  temporaire: 20
	},
	methods: {
	  data_change: function(nb){  // update the number of data
		if (nb>0) {  // security, we don't want negatif number
			this.nb_data = nb;
		}
	  }
	}
  });

  var requete = new Vue({
	el: "#requete",
	data: {  // init of variable
		adresse: "",
		begin_date: date.date_list[0].date,
		end_date: date.date_list[1].date,
		case_name: [],
		nb_data: option.nb_data,
		selector_test: option.list_test[0],
		selector_pod: option.list_pod[0],
		error_message: ""
	},
	methods: {
		update_requete: function(){  // update of variable
			this.begin_date = date.date_list[0].date;  // limit date (begin)
			this.end_date = date.date_list[1].date;  // limit date(end, and more recent)
			this.case_name = case_name.choice;  // name of the test
			this.nb_data = option.nb_data;  // number of data wanted
			this.selector_test = option.selector_test;  // option for the curve
			this.selector_pod = option.selector_pod;  // the CI chain wanted
			this.create_request();  // further operations
		},

		create_request: function(){  // test if all of the options have been filled, and create the HTML request, or raise an error
			if (this.nb_data > 0 && this.case_name.length > 0 && this.begin_date < this.end_date) {  // test if all informations seem correcte
				reset_curve();  // clean graph

				// add a test for communication (can be use as an indicator when the pod load)
				api.ping_pod("https://result2ts.onap.eu/Ping");

				for (i in this.case_name) {  // request and find data for each case wanted
					this.adresse = "https://result2ts.onap.eu/Diagrams?begin_date=" + date_to_string(this.begin_date) + "&end_date=" + date_to_string(this.end_date) + "&case_name=" + this.case_name[i] + "&limit_nb=" + this.nb_data.toString() + "&option=" + this.selector_test.id + "&pod_name=" + this.selector_pod.id;  // creation of the HTTP request
					this.error_message = "";  // clean form error

						console.log(this.adresse);
						api.get_data(this.adresse, this.case_name[i], this.selector_pod.description, this.selector_test.description);  // send the request to the interface
					}
			}
			else {
				this.adresse = "";
				this.error_message = "";  // init error message
				if (this.nb_data <= 0){
					this.error_message += "error with the data number  ";
				}
				if (this.case_name.length < 1){
					this.error_message += "error with the case name  ";
				}
				if (this.begin_date >= this.end_date){
					this.error_message += "error with the date  ";
				}
			}
		}
	}
  });

  var api = new Vue({  // part made to contact the API on k8s
		el: '#api',
		data: {
			info: null,
			tempo: []  // contain the case_name of request send but without info receive
		},
		methods: {
			get_data: function(adresse, case_name, pod_description, test_description) {  // call the server
				this.info = null;
				axios
				  .get(adresse)
				  .then(response => (this.info = this.obtain_response(response, case_name, pod_description, test_description)))  // launch a function to work on fresh data
				  .catch(error => console.log(error));
				console.log("Waiting for data...");
				this.tempo.push(case_name);  // add the case_name to the waiting list
		  },

			obtain_response: function(response, case_name, pod_description, test_description) {
				console.log("end of the download for " + case_name);
				data = response.data;
				get_curve(data.begin_date, data.end_date, api.format_data(data, data.begin_date), case_name, pod_description, test_description);  // send data to the graph
				for (i in this.tempo) {
					if (this.tempo[i] == case_name) {  // remove the case name in the waiting list
						this.tempo.splice(i, 1);
					}
				}
				return response;
			},

		  ping_pod: function(adresse) {  // test the k8s pod
			axios
				  .get(adresse)
				  .then(response => (this.obtain_ping(response)))
				  .catch(error => console.log(error));
				console.log("Waiting for a pod answer...");
				this.tempo.push("pod");  // add "pod" to the waiting list
		  },

		  obtain_ping: function(reponse) {
			console.log("Ping success.");
				for (i in this.tempo) {
					if (this.tempo[i] == "pod") {  // remove "pod" from the waiting list
						this.tempo.splice(i, 1);
					}
				}
		  },

		  format_data: function(entry, begin_date) {  // transform the json data into ts readable by chart.js
			time = new Date(begin_date);  // variable init, at midnight the first day in data
			time.setHours(0);
				time.setMinutes(0);
				time.setSeconds(0);

			data = entry.message;
			data = data.slice(2, -2);
			data = data.split('], [');  // split the data between the date(x) and the purcent of success(y)
			x_list = data[0];
			y_list = data[1];
			x_list = x_list.split(', ');  // make it as a list
			y_list = y_list.split(', ');
			coord = [];
			for (i = 0; i<x_list.length; i++) {  // build of the dictionary list
				if (i === 0) {  // first case (special care)
					x_element = date_to_string(time);
				} else {
						if (parseInt(x_list[i], 10) == parseInt(x_list[i-1], 10)) {  // if it is the same day
							calcul = parseInt(86400000 * (x_list[i] - parseInt(x_list[i-1], 10)), 10);  // add only a gap of a few hours
							gap = new Date(time);
							gap.setTime(gap.getTime() + calcul);
							x_element = date_to_string(gap);
						} else {  // no data left for the day,
							time.setHours(0);  // we go to the next day, at midnight
							time.setMinutes(0);
							time.setSeconds(0);
							time = new Date(time.setDate(time.getDate() + (parseInt(x_list[i], 10) - parseInt(x_list[i-1], 10))));
							x_element = date_to_string(time);
						}
					}

				y_element = parseFloat(y_list[i]);  // add the percent of success
				coord.push({ x: x_element, y: y_element});
			}
			return coord;
		  }
		}
	});

	var information = new Vue({  // part made to contact the "detail" part of the API on k8s
		el: "#information",
		data: {  // init of variable
			legend: ["date", "pods failed", "core (% success)", "core pods failed details", "full (% success)", "full pods failed details", "healthdist"],
			show: false,  // show the data table
			wait: false,  // waiting animation
			data: null
		},
		methods: {
			obtain_response: function(response) {
				this.data = response.data.message;
				this.wait = false;  // hide the loader
				this.show = true;  // show the table
				console.log("Information obtained");
			},
			update_requete: function(){
				adresse = "https://result2ts.onap.eu/Detail?begin_date=" + date_to_string(date.date_list[0].date) + "&end_date=" + date_to_string(date.date_list[1].date) + "&limit_nb=" + option.nb_data.toString() + "&pod_name=" + option.selector_pod.id;  // creation of the HTTP request
				axios
					  .get(adresse)
					  .then(response => (this.obtain_response(response)))  // launch a function to work on fresh data
					  .catch(error => console.log(error));
					console.log("Request for more information.");
					this.wait = true;
			},
			format: function(element) {  // standardize all element as a list
				if (typeof element == "object") {
					return element;
				}
				return [element];
			}
		}
	});

	var chart_config = new Vue({  // interactif button to config the zoom on the graph
		el: '#chart_config',
		data: {
			selection: null,  // button pressed
			option: [{text: "Zoom in"}, {text: "Scroll"}, {text: "Resize"}]  // list of option
		},
	methods: {
		change: function(element) {  // case management (depend on this.option)
			if (element != this.selection) {
				this.selection = element;
				if (this.selection == this.option[0]) {  // enable zoom and disable scrolling
					window.myLine.options.plugins.zoom.pan.enabled = false;
					window.myLine.options.plugins.zoom.zoom.enabled = true;
					window.myLine.options.plugins.zoom.zoom.drag = {
					borderColor: 'rgba(225,225,225,0.3)',
							borderWidth: 5,
							backgroundColor: 'rgb(225,225,225)'
						};
					window.myLine.update();
				}
				else {
					if (this.selection == this.option[1]){  // enable scrolling and disable zoom
						window.myLine.options.plugins.zoom.pan.enabled = true;
						window.myLine.options.plugins.zoom.zoom.enabled = false;
						window.myLine.options.plugins.zoom.zoom.drag = false;
							window.myLine.update();
					}
					else {  // for option[2]
						window.myLine.resetZoom();  // reset zoom
						window.myLine.options.plugins.zoom.zoom.enabled = false;  // no active option
						window.myLine.options.plugins.zoom.zoom.drag = false;
						window.myLine.options.plugins.zoom.pan.enabled = false;
						window.myLine.update();
						this.selection = null;
					}
				}
			}
			else {
				this.selection = null;  // disable all button
				window.myLine.options.plugins.zoom.zoom.enabled = false;  // no active option
					window.myLine.options.plugins.zoom.zoom.drag = false;
					window.myLine.options.plugins.zoom.pan.enabled = false;
					window.myLine.update();
				}
			}
		}
	});

	window.myLine = new Chart(document.getElementById("line-chart"), {  // chart initialisation
		type: 'line',
		data: {
			datasets: []
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'ONAP test'
			},
			tooltips: {
				mode: 'x',
				intersect: false
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {  // add option for axis label
				xAxes: [{
					type: 'time',
					time: {
						parser: 'YYYY-MM-DD HH:mm',  // specify the date format
						// round: 'day',
						tooltipFormat: 'll'
					},
					display: true,
					scaleLabel: {
						display: true,
						labelString: ""
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Percent of success.'
					}
				}]
			},
			plugins: {
				zoom: {  // special parameters for the zoom plugin
					pan: {
						enabled: false,
						mode: 'xy'
					},
					zoom: {
						enabled: false,
						mode: 'xy',
				drag: false
					}
				}
			}
		}
	});
};


var first_date = function(date1, date2) {  // return the first date from the two given (date as string)
	if (string_to_date(date1) - string_to_date(date2) < 0) {
	return date1;
  }
  else {
	return date2;
  }
};

var last_date = function(date1, date2) {  // return the last date from the two given (date as string)
	if (string_to_date(date1) - string_to_date(date2) > 0) {
	return date1;
  }
  else {
	return date2;
  }
};

var date_to_string = function(date) {  // transform date into string (yyyy-mm-dd hh:mm:ss)
	d = date;
  month = '' + (d.getMonth() + 1);
  day = '' + d.getDate();
  year = '' + d.getFullYear();
  hour = '' + d.getHours();
  minute = '' + d.getSeconds();
  second = '' + d.getMinutes();
	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;
	if (hour.length < 2) hour = '0' + hour;
	if (minute.length < 2) minute = '0' + minute;
	if (second.length < 2) second = '0' + second;
	list1 = [year, month, day].join('-');
	list2 = [hour, minute, second].join(":");
	return list1 + " " + list2;
};

var string_to_date = function(string) {  // transform string into javascript object "Date"
	temp = string;
	temp[10]="T";
	date = new Date(temp);
	return date;
};


var global_begin_date = date_to_string(new Date());
var global_end_date = date_to_string(new Date('2010-01-01T00:00:00Z'));

var get_curve = function(begin_date, end_date, coord, type, pod_name, selection){  // function to upgrade the chart
	global_begin_date = first_date(global_begin_date, begin_date);
	global_end_date = last_date(global_end_date, end_date);
	window.myLine.options.scales.xAxes[0].scaleLabel.labelString = "From " + global_begin_date + " to " + global_end_date + " (in day)";
	window.myLine.options.title.text = "ONAP test" + " on " + pod_name + " : " + selection;
	window.myLine.data.datasets.push({
				label: "Case name: " + type,
				data: coord,
				fill: false,
				borderColor: ["#e68a00", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850", "#e68a00", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"][window.myLine.data.datasets.length]
			});
	window.myLine.update();
};

var reset_curve = function() {
	global_begin_date = date_to_string(new Date());  // reset global and graph
  global_end_date = date_to_string(new Date('2010-01-01T00:00:00Z'));
	window.myLine.options.scales.xAxes[0].scaleLabel.labelString = "";
	window.myLine.options.title.text = "ONAP test";
	window.myLine.data.datasets=[];

	window.myLine.resetZoom();  // reset zoom option

	window.myLine.update();  // update modifications
};
